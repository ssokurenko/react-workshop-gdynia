import React, { useState, useEffect } from 'react'
import UsersList from './components/Users/UsersList'
import UsersHome from './components/Users/UsersHome'
import UserProfile from './components/Users/UserProfile'

import './App.css'
import 'antd/dist/antd.css'
import { Layout } from 'antd'
import { getUsers } from './Api'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
const { Header, Sider, Content } = Layout

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    getUsers()
      .then(response => setUsers(response.data))
      .catch(error => console.error(error))
  }, [])

  return (
    <BrowserRouter>
      <Layout>
        <Header>
          <h1 style={{ color: '#fff' }}>Users</h1>
        </Header>
        <Layout>
          <Sider>
            <UsersList users={users} />
          </Sider>
          <Content>
            <Switch>
              <Route exact path="/" component={UsersHome} />
              <Route path="/user/:userId" component={UserProfile} />
              <Route component={UsersHome} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </BrowserRouter>
  )
}

export default App
