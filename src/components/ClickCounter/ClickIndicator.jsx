import React from 'react'

const styles = {
  fontSize: '3rem',
  color: 'green'
}

const ClickIndictor = ({ numberOfClicks }) => (
  <div style={styles}>{numberOfClicks}</div>
)

export default ClickIndictor
