import React from 'react'

const styles = {
  fontSize: '2rem',
  border: '1px solid black',
  borderRadius: '4px',
  color: '#000',
  padding: '2px 2rem',
  margin: '.5rem 1rem',
  cursor: 'pointer'
}

const ClickButton = ({ label = 'default', callback }) => (
  <span style={styles} onClick={() => callback()}>
    {label}
  </span>
)

export default ClickButton
