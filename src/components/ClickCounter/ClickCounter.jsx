import React, { useState } from 'react'
import PropTypes from 'prop-types'

import ClickIndicator from './ClickIndicator'
import ClickButton from './ClickButton'

const ClickCounter = () => {
  const defaultCountValue = 0

  const [numberOfClicks, setNumberOfClicks] = useState(defaultCountValue)

  const add = () => setNumberOfClicks(numberOfClicks + 1)
  const distract = () =>
    numberOfClicks > 0 && setNumberOfClicks(numberOfClicks - 1)

  return (
    <div>
      <ClickIndicator numberOfClicks={numberOfClicks} />
      <div className="buttons">
        <ClickButton label="+" callback={add} />
        <ClickButton label="-" callback={distract} />
      </div>
    </div>
  )
}

ClickCounter.propTypes = {
  label: PropTypes.string.isRequired,
  maxValue: PropTypes.number
}

export default ClickCounter
