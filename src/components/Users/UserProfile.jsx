import React, { useState, useEffect } from 'react'
import { getUser } from '../../Api'

const styles = {
  padding: '2rem'
}

const UserProfile = ({ match }) => {
  const [userProfile, setUserProfile] = useState({})

  useEffect(() => {
    getUser(match.params.userId)
      .then(response => {
        setUserProfile(response.data[0])
      })
      .catch(error => console.error(error))
  }, [match])
  return (
    <div style={styles}>
      <h1>{userProfile.name}</h1>
      <p>{userProfile.email}</p>
    </div>
  )
}

export default UserProfile
