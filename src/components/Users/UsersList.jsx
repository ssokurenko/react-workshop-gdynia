import React from 'react'
import { List } from 'antd'
import { NavLink } from 'react-router-dom'

const UsersList = ({ users = [] }) => {
  const styles = {
    color: '#fff'
  }

  return (
    <List
      size="large"
      bordered
      dataSource={users}
      renderItem={user => (
        <List.Item style={styles}>
          <NavLink to={`/user/${user.id}`}>{user.name}</NavLink>
        </List.Item>
      )}
    />
  )
}

export default UsersList
