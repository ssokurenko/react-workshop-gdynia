import axios from 'axios'

export const getUsers = () =>
  axios.get('https://jsonplaceholder.typicode.com/users')

export const getUser = userId =>
  axios.get(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
